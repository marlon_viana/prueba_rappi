package marlon.com.myapplication.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.databinding.library.baseAdapters.BR;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.MutableLiveData;

import marlon.com.myapplication.R;

/**
 * @author Marlon Viana on 02/07/2019
 * @email 92marlonViana@gmail.com
 */
public class DialogContacto extends DialogFragment {
    private View v;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


        android.app.AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        ViewDataBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(getContext()), R.layout.dialog_info, null, false);
        binding.setLifecycleOwner(getActivity());
        binding.setVariable(BR.viewModel,this);
        v= binding.getRoot();
        builder.setView(v);

        return builder.create();

    }

    public void clickDissmis(){
        dismiss();
    }

    public void clickLinkedin(){
        openWeb(getString(R.string.url_linkedin));
    }

    public void clickWorkana(){
        openWeb(getString(R.string.url_workana));
    }

    public void clickWhatsapp(){
        openWeb(getString(R.string.url_whatsapp));
    }

    public void clickSkype(){
        openWeb(getString(R.string.url_skype));
    }

    private void openWeb(String url){
        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        startActivity(intent);
    }


}
