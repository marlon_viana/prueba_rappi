package marlon.com.myapplication.fragmentSeries;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.databinding.library.baseAdapters.BR;
import androidx.fragment.app.Fragment;

import marlon.com.myapplication.R;
import marlon.com.myapplication.fragmentPeliculas.PeliculasViewModel;

/**
 * @author Marlon Viana on 03/07/2019
 * @email 92marlonViana@gmail.com
 */
public class SeriesView extends Fragment {
    private View v;
    private SeriesViewModel seriesViewModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewDataBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.view_series,container,false);

        v= binding.getRoot();

        seriesViewModel= new SeriesViewModel(getActivity().getApplication(),getActivity());
        binding.setVariable(BR.viewModel,seriesViewModel);
        binding.setLifecycleOwner(getActivity());
        return v;
    }
}
