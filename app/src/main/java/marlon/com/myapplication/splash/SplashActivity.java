package marlon.com.myapplication.splash;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import marlon.com.myapplication.R;
import marlon.com.myapplication.main.MainActivity;

public class SplashActivity extends AppCompatActivity {
    private ImageView imageViewLogin;
    private TextView textViewTitle,textViewDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        maxScreen();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        addView();

        new Handler().postDelayed(()->{goToMain();}, 5000);
    }

    private void maxScreen(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);

            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);

        } else{
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
    }

    private void addView(){
        Animation fadeIn = AnimationUtils.loadAnimation(this,R.anim.fade_in);
        Animation zoonIn= AnimationUtils.loadAnimation(this,R.anim.logo_splash);

        imageViewLogin= findViewById(R.id.img_logo);
        imageViewLogin.startAnimation(zoonIn);

        textViewTitle= findViewById(R.id.txt_title);
        textViewTitle.setVisibility(View.INVISIBLE);
        textViewDescription= findViewById(R.id.txt_descripcion);
        textViewDescription.setVisibility(View.INVISIBLE);

        new Handler().postDelayed(()->{
            textViewTitle.setVisibility(View.VISIBLE);
            textViewDescription.setVisibility(View.VISIBLE);
            textViewTitle.startAnimation(fadeIn);
            textViewDescription.startAnimation(fadeIn);
        }, 3500);

    }

    private void goToMain(){
        Intent intent= new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);

    }
}
