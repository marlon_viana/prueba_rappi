package marlon.com.myapplication.fragmentPeliculas;

import android.app.Activity;
import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

/**
 * @author Marlon Viana on 03/07/2019
 * @email 92marlonViana@gmail.com
 */
public class PeliculasViewModel extends AndroidViewModel {
  private Application application;
  private Activity activity;

  public PeliculasViewModel(Application application, Activity activity) {
    super(application);
    this.application= application;
    this.activity = activity;
  }


}
