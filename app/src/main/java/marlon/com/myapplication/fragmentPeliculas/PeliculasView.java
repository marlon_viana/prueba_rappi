package marlon.com.myapplication.fragmentPeliculas;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.databinding.library.baseAdapters.BR;
import androidx.fragment.app.Fragment;

import marlon.com.myapplication.R;

/**
 * @author Marlon Viana on 03/07/2019
 * @email 92marlonViana@gmail.com
 */
public class PeliculasView extends Fragment {
    private View v;
    private PeliculasViewModel peliculasViewModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ViewDataBinding binding = DataBindingUtil.inflate(inflater,
                R.layout.view_peliculas,container,false);

        v= binding.getRoot();

        peliculasViewModel= new PeliculasViewModel(getActivity().getApplication(),getActivity());
        binding.setVariable(BR.viewModel,peliculasViewModel);
        binding.setLifecycleOwner(getActivity());
        return v;
    }
}
