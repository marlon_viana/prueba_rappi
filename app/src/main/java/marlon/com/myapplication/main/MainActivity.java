package marlon.com.myapplication.main;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.databinding.library.baseAdapters.BR;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import com.google.android.material.bottomappbar.BottomAppBar;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.navigation.NavigationView;

import marlon.com.myapplication.R;
import marlon.com.myapplication.dialog.DialogContacto;
import marlon.com.myapplication.fragmentPeliculas.PeliculasView;
import marlon.com.myapplication.fragmentSeries.SeriesView;

/**
 * @author Marlon Viana on 02/07/2019
 * @email 92marlonViana@gmail.com
 */
public class MainActivity extends AppCompatActivity {
    private FragmentManager fragmentManager;
    private FragmentTransaction fragmentTransaction;
    private PeliculasView peliculasView;
    private SeriesView seriesView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ViewDataBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        binding.setVariable(BR.viewModel, this);
        binding.setLifecycleOwner(this);

        addView();
        initFragment();
        addSeries();
    }

    private void addView() {
        BottomAppBar bar= findViewById(R.id.bar);
        bar.setNavigationOnClickListener((v)->{
            showDialogInfo();
        });
    }

    private void initFragment() {
        fragmentManager= getSupportFragmentManager();
        fragmentTransaction=fragmentManager.beginTransaction();

        peliculasView= new PeliculasView();
        seriesView= new SeriesView();

    }

    public void openMenu(){
        final View bootomNavigation = getLayoutInflater().inflate(R.layout.navigation_menu,null);
        BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(MainActivity.this);
        bottomSheetDialog.setContentView(bootomNavigation);
        bottomSheetDialog.show();

        NavigationView navigationView = bootomNavigation.findViewById(R.id.navigation_menu);

        navigationView.setNavigationItemSelectedListener(menuItem -> {
            int id = menuItem.getItemId();
            switch (id){
                case R.id.item_series:
                    addSeries();
                    bottomSheetDialog.dismiss();
                    break;
                case R.id.item_peliculas:
                    addPeliculas();
                    bottomSheetDialog.dismiss();
                    break;

            }
            return false;
        });
    }




    private void addSeries(){
        fragmentTransaction = fragmentManager.beginTransaction();
        if (fragmentManager.findFragmentByTag("FM_SERIES")==null){
            fragmentTransaction.setCustomAnimations(R.anim.fade_in,R.anim.fade_out);
            fragmentTransaction.replace(R.id.fm_main, seriesView,"FM_SERIES");
            fragmentTransaction.commit();
        }

    }

    private void addPeliculas(){
        fragmentTransaction = fragmentManager.beginTransaction();
        if (fragmentManager.findFragmentByTag("FM_PELICULAS")==null){
            fragmentTransaction.setCustomAnimations(R.anim.fade_in,R.anim.fade_out);
            fragmentTransaction.replace(R.id.fm_main, peliculasView,"FM_PELICULAS");
            fragmentTransaction.commit();
        }
    }

    private void showDialogInfo(){
        DialogContacto dialog= new DialogContacto();
        dialog.show(getSupportFragmentManager(),"DIALOG_INFO");
        dialog.setCancelable(false);
    }

}
